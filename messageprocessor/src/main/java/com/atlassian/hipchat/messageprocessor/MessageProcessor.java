package com.atlassian.hipchat.messageprocessor;

import com.atlassian.hipchat.messageprocessor.tokenizer.TokenizerListener;
import com.atlassian.hipchat.messageprocessor.tokenizer.recognisers.impl.EmoticonRecogniser;
import com.atlassian.hipchat.messageprocessor.tokenizer.recognisers.impl.MentionRecogniser;
import com.atlassian.hipchat.messageprocessor.tokenizer.recognisers.impl.UrlRecogniser;
import com.atlassian.hipchat.messageprocessor.tokenizer.Tokenizer;
import com.atlassian.hipchat.messageprocessor.util.JsonOutputTokenizerListener;

import java.util.concurrent.Callable;

import rx.Observable;

/**
 * A MessageProcessor exposes an asynchronous interface allowing a JSON string representation of any emoticons, mentions
 * and links to be extracted from a string.
 *
 * @author Jeffrey Thompson
 */
public class MessageProcessor {
    /**
     * @return a Tokenizer configured to recognize Emoticons, Mentions and Links
     */
    private static Tokenizer createTokenizer() {
        Tokenizer tokenizer = new Tokenizer();
        tokenizer.addRecogniser(new EmoticonRecogniser());
        tokenizer.addRecogniser(new MentionRecogniser());
        tokenizer.addRecogniser(new UrlRecogniser());
        return tokenizer;
    }

    /**
     * @param chatMessage the String message to look for mentions, emoticons and links in
     * @return A JSON representation of the mentions, emoticons and links in the @param(chatMessage)
     */
    private static String extractChatMessageJson(String chatMessage) {
        Tokenizer tokenizer = createTokenizer();
        TokenizerListener tokenizerListener = new JsonOutputTokenizerListener(false);
        tokenizer.setListener(tokenizerListener);
        tokenizer.tokenize(chatMessage);

        return tokenizerListener.toString();
    }

    /**
     * @param chatMessage the String message to look for mentions, emoticons and links in
     * @return A JSON representation of the mentions, emoticons and links in the @param(chatMessage) with pretty formatting
     */
    private static String extractChatMessageJsonPretty(String chatMessage) {
        Tokenizer tokenizer = createTokenizer();
        TokenizerListener tokenizerListener = new JsonOutputTokenizerListener(true);
        tokenizer.setListener(tokenizerListener);
        tokenizer.tokenize(chatMessage);

        return tokenizerListener.toString();
    }

    /**
     * @param chatMessage the String message to look for mentions, emoticons and links in
     * @return An observable that, when complete, will provide a JSON representation of the mentions,
     * emoticons and links in the @param(chatMessage)
     */
    public Observable<String> extractChatMessageJsonAsync(final String chatMessage) {
        return Observable.fromCallable(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return extractChatMessageJson(chatMessage);
            }
        });
    }

    /**
     * @param chatMessage the String message to look for mentions, emoticons and links in
     * @return An observable that, when complete, will provide a JSON representation of the mentions,
     * emoticons and links in the @param(chatMessage) pretty printed with indentation
     */
    public Observable<String> extractChatMessageJsonPrettyAsync(final String chatMessage) {
        return Observable.fromCallable(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return extractChatMessageJsonPretty(chatMessage);
            }
        });
    }
}
