package com.atlassian.hipchat.messageprocessor.util;

import com.atlassian.hipchat.messageprocessor.tokenizer.Token;
import com.atlassian.hipchat.messageprocessor.tokenizer.TokenizerListener;

import java.util.LinkedList;

/**
 * A linked list that Listens to the tokenizer for recognised tokens and adds them to itself.
 */
public class LinkedListTokenizerListener extends LinkedList<Token> implements TokenizerListener {
    @Override
    public void notify(Token recognisedToken) {
        add(recognisedToken);
    }
}
