package com.atlassian.hipchat.messageprocessor.util;

import android.util.JsonWriter;

import com.atlassian.hipchat.messageprocessor.tokenizer.Token;
import com.atlassian.hipchat.messageprocessor.tokenizer.TokenizerListener;
import com.atlassian.hipchat.messageprocessor.tokenizer.recognisers.impl.EmoticonRecogniser;
import com.atlassian.hipchat.messageprocessor.tokenizer.recognisers.impl.MentionRecogniser;
import com.atlassian.hipchat.messageprocessor.tokenizer.recognisers.impl.UrlRecogniser;

import java.io.IOException;
import java.io.StringWriter;
import java.util.LinkedList;

//now that this is in Android land I would prefer to refactor it out and use GSON
@SuppressWarnings("TryFinallyCanBeTryWithResources") // we'll use try/finally for compatibility
public class JsonOutputTokenizerListener implements TokenizerListener {
    private final LinkedList<MentionRecogniser.Mention> mentions = new LinkedList<>();
    private final LinkedList<EmoticonRecogniser.Emoticon> emoticons = new LinkedList<>();
    private final LinkedList<UrlRecogniser.Link> links = new LinkedList<>();
    private final boolean pretty;

    public JsonOutputTokenizerListener(boolean pretty) {
        this.pretty = true;
    }

    @Override
    public void notify(Token token) {
        Object recognisedToken = token.recognisedToken;

        if (MentionRecogniser.Mention.class.isInstance(recognisedToken)) {
            mentions.add((MentionRecogniser.Mention) recognisedToken);
        } else if (EmoticonRecogniser.Emoticon.class.isInstance(recognisedToken)) {
            emoticons.add((EmoticonRecogniser.Emoticon) recognisedToken);
        } else if (UrlRecogniser.Link.class.isInstance(recognisedToken)) {
            links.add((UrlRecogniser.Link) recognisedToken);
        }
    }

    @Override
    public String toString() {
        try {
            StringWriter output = new StringWriter();
            try {
                JsonWriter writer = new JsonWriter(output);
                writer.setIndent("  ");
                try {
                    writer.beginObject();
                    if (!mentions.isEmpty()) {
                        writeMentions(writer);
                    }
                    if (!emoticons.isEmpty()) {
                        writeEmoticons(writer);
                    }

                    if (!links.isEmpty()) {
                        writeLinks(writer);
                    }
                    writer.endObject();
                } finally {
                    writer.close();
                }
                return output.toString();
            } finally {
                output.close();
            }
        } catch (IOException e) {
            //error serializing. return null.
            Logger.warning("IO Exception trying to serialize chat message. Returning empty string.", e);
            return "";
        }
    }

    private void writeLinks(JsonWriter writer) throws IOException {
        writer.name("links");
        writer.beginArray();
        for (UrlRecogniser.Link u : links) {
            writer.beginObject();
            writer.name("link");
            writer.value(u.getUrl());
            writer.name("title");
            writer.value(u.getTitle());
            writer.endObject();
        }
        writer.endArray();
    }

    private void writeEmoticons(JsonWriter writer) throws IOException {
        writer.name("emoticons");
        writer.beginArray();
        for (EmoticonRecogniser.Emoticon m : emoticons) {
            writer.value(m.getMoniker());          //       "success"
        }
        writer.endArray();                          //    ],
    }

    private void writeMentions(JsonWriter writer) throws IOException {
        writer.name("mentions");                    //
        writer.beginArray();                        //
        for (MentionRecogniser.Mention m : mentions) {                //
            writer.value(m.getUserName());          //       "john"
        }                                           //
        writer.endArray();                          //    ],
    }
}
