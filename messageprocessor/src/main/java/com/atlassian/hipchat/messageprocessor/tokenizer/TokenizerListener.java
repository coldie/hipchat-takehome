package com.atlassian.hipchat.messageprocessor.tokenizer;

public interface TokenizerListener {
    void notify(Token recognisedToken);
}
