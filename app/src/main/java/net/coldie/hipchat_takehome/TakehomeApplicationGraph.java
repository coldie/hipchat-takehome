package net.coldie.hipchat_takehome;

import net.coldie.hipchat_takehome.data.MessageRepositoryModule;
import net.coldie.hipchat_takehome.services.MessageProcessorModule;
import net.coldie.hipchat_takehome.ui.activities.MainActivity;
import net.coldie.hipchat_takehome.ui.MessagesRecyclerViewModule;

import javax.inject.Singleton;

import dagger.Component;

@SuppressWarnings("unused")
@Component(modules = {TakehomeApplicationModule.class, MessagesRecyclerViewModule.class, MessageProcessorModule.class, MessageRepositoryModule.class})
@Singleton
public interface TakehomeApplicationGraph {
    void inject(MainActivity activity);
}
