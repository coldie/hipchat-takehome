package net.coldie.hipchat_takehome.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.widget.EditText;

import com.atlassian.hipchat.messageprocessor.MessageProcessor;

import net.coldie.hipchat_takehome.R;
import net.coldie.hipchat_takehome.TakehomeApplication;
import net.coldie.hipchat_takehome.model.MessageViewModel;
import net.coldie.hipchat_takehome.ui.adapters.MessageListAdapter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity {
    private static final java.lang.String ALREADY_SEEN_INTRO_KEY = "ALREADY_SEEN_INTRO_KEY";

    @Inject
    protected MessageProcessor messageProcessor;

    @Inject
    protected MessageListAdapter messageListAdapter;

    @Inject
    protected RecyclerView.LayoutManager messagesLayoutManager;

    @Inject
    protected ItemTouchHelper itemTouchHelper;

    @Bind(R.id.message_input)
    protected EditText editText;

    @Bind(R.id.messages_view)
    protected RecyclerView messagesRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        TakehomeApplication.get(this).getGraph().inject(this);

        messagesRecyclerView.setLayoutManager(messagesLayoutManager);
        messagesRecyclerView.setAdapter(messageListAdapter);
        itemTouchHelper.attachToRecyclerView(messagesRecyclerView);

        if (!(savedInstanceState != null && savedInstanceState.getBoolean(ALREADY_SEEN_INTRO_KEY))) {
            processMessage(getString(R.string.intro_message));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(ALREADY_SEEN_INTRO_KEY, true);
        super.onSaveInstanceState(outState);
    }

    @OnClick(R.id.fab)
    protected void actionButtonClicked() {
        String text = editText.getText().toString();

        Timber.d("editText: %s", text);

        processMessage(text);
    }

    private void processMessage(String text) {
        MessageViewModel model = new MessageViewModel();
        model.originalMessage = text;
        model.jsonResponse = messageProcessor.extractChatMessageJsonPrettyAsync(text)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .cache();

        messageListAdapter.addItem(model);
    }
}
