package net.coldie.hipchat_takehome.ui;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import net.coldie.hipchat_takehome.data.MessageRepository;
import net.coldie.hipchat_takehome.ui.adapters.MessageListAdapter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class MessagesRecyclerViewModule {
    @Provides
    public RecyclerView.LayoutManager provideLayoutManager(Context context) {
        return new LinearLayoutManager(context);
    }

    @Provides
    @Singleton
    public MessageListAdapter provideMessageListAdapter(MessageRepository messages) {
        return new MessageListAdapter(messages);
    }

    @Provides
    @Singleton
    public ItemTouchHelper provideItemTouchHelper(final ItemTouchHelper.SimpleCallback itemTouchCallback) {
        return new ItemTouchHelper(itemTouchCallback);
    }

    @Provides
    @Singleton
    public ItemTouchHelper.SimpleCallback provideRemoveItemOnSwipeCallback(final MessageListAdapter adapter) {

        return new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                adapter.remove(viewHolder.getAdapterPosition());
            }
        };
    }


}
