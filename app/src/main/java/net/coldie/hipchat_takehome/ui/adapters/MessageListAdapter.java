package net.coldie.hipchat_takehome.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ViewAnimator;

import net.coldie.hipchat_takehome.data.MessageRepository;
import net.coldie.hipchat_takehome.model.MessageViewModel;
import net.coldie.hipchat_takehome.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import timber.log.Timber;

/**
 * Manages a list of parsed message view models in a recycler view.
 *
 * When bound to a view holder, subscribes to an observable of the JSON response and then
 * updates the content in the view item appropriately.
 */
public class MessageListAdapter extends RecyclerView.Adapter<MessageListAdapter.MessageViewHolder> {
    private final MessageRepository messages;

    public MessageListAdapter(MessageRepository messages) {
        this.messages = messages;
    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_message_item, parent, false);
        return new MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MessageViewHolder holder, int position) {
        //show the loading animation while we await a result from the observable for this item
        holder.viewAnimator.setDisplayedChild(0);
        MessageViewModel item = messages.get(position);
        holder.inputText.setText(item.originalMessage);

        //subscribe to the json response observable and update the item view when it's finished
        item.jsonResponse
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        // error: show our embarrassing error view
                        Timber.e(throwable, "Error parsing message");
                        holder.viewAnimator.setDisplayedChild(2);
                    }
                })
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        //show our content view
                        holder.viewAnimator.setDisplayedChild(1);
                        holder.resultText.setText(s);
                    }
                });
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public void addItem(MessageViewModel model) {
        messages.add(model);
        notifyItemInserted(messages.size()-1);
    }

    public void remove(long itemId) {
        messages.remove((int)itemId);
        notifyItemRemoved((int)itemId);
    }

    public static class MessageViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.viewAnimator)
        ViewAnimator viewAnimator;

        @Bind(R.id.original_input)
        TextView inputText;

        @Bind(R.id.result_text)
        TextView resultText;

        public MessageViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
