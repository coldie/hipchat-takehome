package net.coldie.hipchat_takehome.services;

import com.atlassian.hipchat.messageprocessor.MessageProcessor;

import dagger.Module;
import dagger.Provides;

/**
 * Provides access to the Message Processor that we import from our library module
 */
@Module
public class MessageProcessorModule {
    @Provides
    MessageProcessor provideMessageProcessor() {
        return new MessageProcessor();
    }
}
