package net.coldie.hipchat_takehome;

import android.app.Application;
import android.content.Context;

import net.coldie.hipchat_takehome.services.MessageProcessorModule;
import net.coldie.hipchat_takehome.ui.MessagesRecyclerViewModule;

public class TakehomeApplication extends Application {
    private TakehomeApplicationGraph graph;

    @Override
    public void onCreate() {
        super.onCreate();

        graph = DaggerTakehomeApplicationGraph.builder()
                .takehomeApplicationModule(new TakehomeApplicationModule(this))
                .messageProcessorModule(new MessageProcessorModule())
                .messagesRecyclerViewModule(new MessagesRecyclerViewModule())
                .build();
    }

    public TakehomeApplicationGraph getGraph() {
        return graph;
    }

    public static TakehomeApplication get(Context context) {
        return (TakehomeApplication) context.getApplicationContext();
    }
}
