package net.coldie.hipchat_takehome.model;

import rx.Observable;

/**
 * Models the View of a Message that the user has entered - the string they entered and the
 * observable that will return a JSON response that contains the metadata that the message
 * processor has extracted
 */
public class MessageViewModel {
    public String originalMessage;
    public Observable<String> jsonResponse;
}
