package net.coldie.hipchat_takehome.data;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Provides access to the list of messages that the user has entered
 */
@Module
public class MessageRepositoryModule {
    @Provides
    @Singleton
    public MessageRepository provideMessageRepository() {
        return new MessageRepository();
    }
}
