package net.coldie.hipchat_takehome.data;

import net.coldie.hipchat_takehome.model.MessageViewModel;

import java.util.ArrayList;

/**
 * Trivial ArrayList subclass to roughly encapsulate the list of MessageViewModels
 */
public class MessageRepository extends ArrayList<MessageViewModel> {

}
