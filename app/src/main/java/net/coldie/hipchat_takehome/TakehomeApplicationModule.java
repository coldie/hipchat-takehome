package net.coldie.hipchat_takehome;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

@Module
public class TakehomeApplicationModule {
    private static TakehomeApplication application;

    public TakehomeApplicationModule(TakehomeApplication application) {
        TakehomeApplicationModule.application = application;
    }

    @Provides
    TakehomeApplication providesApplication() {
        return application;
    }

    @Provides
    Context providesContext() {
        return application;
    }
}
