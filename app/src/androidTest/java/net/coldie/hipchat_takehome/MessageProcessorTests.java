package net.coldie.hipchat_takehome;

import android.support.test.runner.AndroidJUnit4;

import com.atlassian.hipchat.messageprocessor.MessageProcessor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import java.util.concurrent.TimeUnit;

import rx.Observable;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class MessageProcessorTests {
    private final static long TIMEOUT_MILLISECONDS = 10000; // 10 second timeout

    @Test
    public void emptyMessage() {
        MessageProcessor processor = new MessageProcessor();

        Observable<String> jsonFuture = processor.extractChatMessageJsonAsync("");

        String result = jsonFuture.toBlocking().first();
        assertEquals("Empty JSON response when empty string passed in", "{}", result);
    }

    @Test
    public void nullMessage() {
        MessageProcessor processor = new MessageProcessor();

        Observable<String> jsonFuture = processor.extractChatMessageJsonAsync(null);

        String result = jsonFuture.toBlocking().first();
        assertEquals("Empty JSON response when null string passed", "{}", result);

    }

    @Test
    public void providedExampleA() {
        MessageProcessor processor = new MessageProcessor();

        Observable<String> jsonFuture =
                processor.extractChatMessageJsonAsync("@chris you around?");

        String result = jsonFuture.timeout(TIMEOUT_MILLISECONDS, TimeUnit.MILLISECONDS).toBlocking().single();
        assertEquals("Provided example 1 works", "{\"mentions\":[\"chris\"]}", result);
    }

    @Test
    public void providedExampleB() {
        MessageProcessor processor = new MessageProcessor();

        Observable<String> jsonFuture =
                processor.extractChatMessageJsonAsync("Good morning! (megusta) (coffee)");

        String result = jsonFuture.timeout(TIMEOUT_MILLISECONDS, TimeUnit.MILLISECONDS).toBlocking().single();
        assertEquals("Provided example 2 works", "{\"emoticons\":[\"megusta\",\"coffee\"]}", result);
    }

    @Test
    public void providedExampleC() {
        MessageProcessor processor = new MessageProcessor();

        Observable<String> jsonFuture =
                processor.extractChatMessageJsonAsync("Olympics are starting soon; http://www.nbcolympics.com");

        String result = jsonFuture.timeout(TIMEOUT_MILLISECONDS, TimeUnit.MILLISECONDS).toBlocking().single();
        assertEquals("Provided example 3 works", "{\"links\":[{\"link\":\"http://www.nbcolympics.com\",\"title\":\"NBC Olympics | Home of the 2016 Olympic Games in Rio\"}]}", result);
    }

    @Test
    public void providedExampleD() {
        MessageProcessor processor = new MessageProcessor();

        Observable<String> jsonFuture =
                processor.extractChatMessageJsonAsync("@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016");

        String result = jsonFuture.timeout(TIMEOUT_MILLISECONDS, TimeUnit.MILLISECONDS).toBlocking().single();
        assertEquals("Provided example 4 works", "{\"mentions\":[\"bob\",\"john\"],\"emoticons\":[\"success\"],\"links\":[{\"link\":\"https://twitter.com/jdorfman/status/430511497475670016\",\"title\":\"Justin Dorfman on Twitter: \\\"nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq\\\"\"}]}", result);
    }
}