package net.coldie.hipchat_takehome;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.runner.AndroidJUnit4;

import net.coldie.hipchat_takehome.ui.activities.MainActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class TakehomeApplicationTest {
    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void defaultCardIsShown() {
        onView(withText("Original Input")).check(matches(isDisplayed()));
        onView(withText("JSON Result")).check(matches(isDisplayed()));
    }

    @Test
    public void canEnterText () {
        onView(withText("Original Input")).check(matches(isDisplayed()));
        onView(withText("JSON Result")).check(matches(isDisplayed()));
    }
}