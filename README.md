# Atlassian Takehome Take 2

Wrapped the chat message parser that I wrote for part 1 of the takehome into an
Android library project and consume it in a straightforward Android app that
demonstrates some best practices.

Yes, there are even UI tests.

There was some refactoring needed for Androidifying the initial takehome
submission as I had targeted Java 8 and of course that's not supported on
Android.

Because my normal toolkit on Android includes RxJava I swapped the
CompletableFuture<> for Observable<>. Actually I really prefer the RxJava
API to Java 8's, the reason I didn't use that in the first place was that I was
trying to keep the number of external dependencies down.

# Requirements

Needs the latest Android SDK and tools installed. I didn't include any gradle
smarts to detect this so if it bombs out, ensure you have SDK tools 24.4.1 and
23.1, and build tools 23.0.2, as well as the Android 6.0 SDK (API 23).

# Getting Started

    ./gradlew clean assembleDebug
    ./gradlew check

# Notes

Used a subset of my standard toolkit. Dagger for DI, RxJava and RxAndroid for
async, ButterKnife for convenient view injection, Espresso2 for UI tests, Timber
for logging. Espresso tests aren't working reliably on this gradle alpha.

I didn't implement persistence or state restoration. You can imagine an
alternative implementation of MessageRepository that persists data locally.